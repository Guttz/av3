import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.avaliacao.Lancamento;
import utfpr.ct.dainf.if62c.avaliacao.ProcessaLancamentos;

public class Avaliacao3 {

    public static void main(String[] args) throws FileNotFoundException, IOException  {
        ProcessaLancamentos processador;
        Scanner scanner = new Scanner(System.in); 
        Integer acc;
        List<Lancamento> lancamentos;

        boolean continua = true;
        
            System.out.println("Caminho:");
       
            String path = scanner.next();
                
            processador = new ProcessaLancamentos(path);
                
            lancamentos = processador.getLancamentos();
                
            continua = false;
                
            System.out.println("Conta:");

            while(!continua){
                    
            if(scanner.hasNextInt()){
                    acc = scanner.nextInt();
                    Avaliacao3.exibeLancamentosConta(lancamentos, acc);
                    continua = true;
                }
                    
                else
                    System.out.println("Por favor, informe um valor numérico");  
                    
                }
        }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        
        List<Lancamento> contaDados = new ArrayList<Lancamento>();
        
        Lancamento LancAux = new Lancamento(conta, null, null, null);
                
        int i = lancamentos.indexOf(LancAux);
        
        if(i >= 0){
            contaDados.add(lancamentos.get(i));
            i++;
            
            while( (lancamentos.get(i)).getConta().equals((lancamentos.get(i-1)).getConta())){
                contaDados.add(lancamentos.get(i));
                i++;
            }
 
            for(Lancamento l: contaDados){
                       System.out.println(l.toString());
                    }
        }    
        else System.out.println("Conta inexistente");
    }
 
}