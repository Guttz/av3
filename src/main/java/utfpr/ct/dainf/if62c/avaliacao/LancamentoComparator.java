package utfpr.ct.dainf.if62c.avaliacao;
import java.util.Comparator;

public class LancamentoComparator implements Comparator<Lancamento>{
    
    @Override
    public int compare(Lancamento Lanc1, Lancamento Lanc2)
    {
        int comp = Lanc1.getConta().compareTo(Lanc2.getConta());
        
        if(comp == 0){
            comp = Lanc1.getData().compareTo(Lanc2.getData());
        }
        
        return comp;
    }
}
