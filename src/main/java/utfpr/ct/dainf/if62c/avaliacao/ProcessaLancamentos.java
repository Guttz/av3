package utfpr.ct.dainf.if62c.avaliacao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(path));
    }
    
    private String getNextLine() throws IOException {
        return reader.readLine();
    }
    
    private Lancamento processaLinha(String linha) {
        String descricao;
        int mes, ano, dia;
        Integer conta;
        double valor;

        conta = Integer.parseInt(linha.substring(0, 6));
        ano = Integer.parseInt(linha.substring(6, 10));
        mes = Integer.parseInt(linha.substring(10, 12));
        dia = Integer.parseInt(linha.substring(12, 14));
        
        descricao = linha.substring(14, 74).trim();
        valor = Double.parseDouble(linha.substring(74, 86))/100;
        
        return new Lancamento(conta, new GregorianCalendar(ano, mes-1, dia).getTime(), descricao, valor);
    }
    
    private Lancamento getNextLancamento() throws IOException {
        String linha = getNextLine();
        
        
        if(linha != null){
            return processaLinha(linha);
        }
        
        else{
            return null;
        }
    }
    
    public List<Lancamento> getLancamentos() throws IOException {
        Lancamento auxiliar;
        List<Lancamento> lancamentos = new ArrayList<>();
        while((auxiliar = getNextLancamento()) != null){
            lancamentos.add(auxiliar);
        }
        reader.close();
        Collections.sort(lancamentos, new LancamentoComparator());
        return lancamentos;
    }
    
}
